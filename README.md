# Ecumene Project

Exercise for Job Interview - Trip Sorter

# About

This application takes input of boarding passes and sort them in ascending order.

## Built With

* [Python](https://www.python.org/) - Backend Language
* [Flask](https://palletsprojects.com/p/flask/) - Web framework

## Installation

#### Clone repository:

```sh
$ git clone https://gitlab.com/R4dziu/ecumene_trip_sorter
```

#### Change directory to main project path:

```sh
$ cd ecumene_trip_sorter
```

#### Build and run Docker image:

```sh
$ docker-compose up --build
```

## API

### POST:

#### Create 1 trip with multiple boarding passes:
URL:
```sh
 http://127.0.0.1:5000/api/v1/trips/
```

INPUT:
```sh
 {
	"name": "poland_trip",
	"boarding_pass":
	[
		{
			"origin": "warszawa",
			"destination": "gdansk",
			"mean_of_transport": "bus"
		},
		{
			"origin": "krakow",
			"destination": "wroclaw",
			"mean_of_transport": "train",
			"seat": "123"
		},
		{
			"origin": "gdansk",
			"destination": "krakow",
			"mean_of_transport": "airplane",
			"seat": "22A",
			"gate": "5A"
		}
	]
}
```

### GET:

#### Get all trips:
URL:
```sh
 http://127.0.0.1:5000/api/v1/trips/
```

#### Get 1 trip by id:
URL:
```sh
 http://127.0.0.1:5000/api/v1/trips/{trip.id}
```

