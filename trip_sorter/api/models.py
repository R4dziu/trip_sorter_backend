# from datetime import datetime
from api import db


class Trip(db.Model):
    __tablename__ = "trip"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    boarding_passes = db.relationship('BoardingPass', backref='trip')

    def __repr__(self):
        return "<Trip %r>" % self.name


class MeanOfTransport(db.Model):
    __tablename__ = "mean_of_transport"
    id = db.Column(db.Integer, primary_key=True)
    type_of_transport = db.Column(db.String(64))
    boarding_passes = db.relationship('BoardingPass',
                                      backref='mean_of_transport')

    def __repr__(self):
        return "<MeanOfTransport %r>" % self.type_of_transport


class BoardingPass(db.Model):
    __tablename__ = "boarding_pass"
    id = db.Column(db.Integer, primary_key=True)
    trip_id = db.Column(db.Integer, db.ForeignKey("trip.id"))
    order = db.Column(db.Integer)
    mean_of_transport_id = db.Column(db.Integer,
                                     db.ForeignKey("mean_of_transport.id"))
    origin = db.Column(db.String(64))
    destination = db.Column(db.String(64))
    seat = db.Column(db.String(64), nullable=True)
    gate = db.Column(db.String(64), nullable=True)

    def to_json(self):
        json_model = {
            'mean_of_transport': self.mean_of_transport_id,
            'order': self.order,
            'origin': self.origin,
            'destination': self.destination,
            'seat': self.seat,
            'gate': self.gate
        }

        return json_model
