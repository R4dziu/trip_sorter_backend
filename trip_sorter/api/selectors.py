from flask import jsonify
from api.models import Trip, BoardingPass
from api import db


def serialize_trip(trip):
    boarding_passes = []
    for bp in trip.boarding_passes:
        boarding_passes.append(BoardingPass.to_json(bp))

    result = {
        'id': trip.id,
        'name': trip.name,
        'boarding_passes': boarding_passes
    }

    return result


def get_all_trips():
    trips = db.session.query(Trip).all()

    list_of_trips = []
    for trip in trips:
        list_of_trips.append(serialize_trip(trip))

    return jsonify({'list_of_trips': list_of_trips})


def get_trip(id):
    trip = db.session.query(Trip).filter_by(id=id).first()
    result = serialize_trip(trip)
    return jsonify(result)
