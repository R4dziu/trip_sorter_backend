from flask import abort
from utils.sorter import Sorter
from api.models import Trip, BoardingPass, MeanOfTransport
from api import db


def initialize_mean_of_transport():
    table_exists = db.engine.dialect.has_table(db.engine, 'mean_of_transport')
    if table_exists is False or MeanOfTransport.query.first() is None:
        try:
            db.session.add(MeanOfTransport(type_of_transport='bus'))
            db.session.add(MeanOfTransport(type_of_transport='train'))
            db.session.add(MeanOfTransport(type_of_transport='airplane'))
            db.session.commit()
        except Exception as e:
            print("Cannot add this mean of transport: {}".format(e))


def post_trip(request):
    initialize_mean_of_transport()
    try:
        trip_data = request.get_json()
        sorter = Sorter()
        trip_data['boarding_pass'] = sorter.sort(trip_data['boarding_pass'])

        trip = Trip(name=trip_data.get('name'))
        db.session.add(trip)
        db.session.commit()

        bp_data = trip_data.get('boarding_pass')
        i = 1

        for bp in bp_data:
            boarding_pass = BoardingPass(
                trip_id=trip.id,
                order=i,
                mean_of_transport_id=bp.get('mean_of_transport'),
                origin=bp.get('origin'),
                destination=bp.get('destination'),
                seat=bp.get('seat'),
                gate=bp.get('gate')
            )

            db.session.add(boarding_pass)
            db.session.commit()
            i += 1

        return trip_data

    except Exception as e:
        print("Cannot add this trip: {}".format(e))
        return abort(400)
