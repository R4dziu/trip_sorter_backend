from flask import request, abort
from api import app
from api import selectors, services

api_url = '/api/v1/'


@app.route(api_url + 'trips/', methods=['GET'])
def get_trips():
    result = selectors.get_all_trips()

    return result


@app.route(api_url + 'trips/<int:id>', methods=['GET'])
def get_trip_details(id):
    result = selectors.get_trip(id)

    return result


@app.route(api_url + 'trips/', methods=['POST'])
def create_trip():
    if not request.json:
        abort(400)

    result = services.post_trip(request)
    print(result)

    return result
