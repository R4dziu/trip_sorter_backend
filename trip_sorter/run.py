import os
from api import app, db

if __name__ == "__main__":
    if not os.path.isfile("data.sqlite"):
        db.create_all()
    app.run(debug=True, host="0.0.0.0")
