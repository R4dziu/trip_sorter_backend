import unittest
from api.models import Trip, BoardingPass, MeanOfTransport


class BoardingPassTests(unittest.TestCase):

    def setUp(self):
        self.trip = Trip(id=1, name='Poland Trip')
        self.mean_of_transport = MeanOfTransport(id=3,
                                                 type_of_transport='airplane')

        self.boarding_pass = BoardingPass(id=1,
                                          trip_id=self.trip.id,
                                          order=1,
                                          mean_of_transport_id=3,
                                          origin='warszawa',
                                          destination='gdansk',
                                          seat='41A',
                                          gate='23B')

    def test_to_json(self):
        expected_value = {
          'mean_of_transport': 3,
          'order': 1,
          'origin': 'warszawa',
          'destination': 'gdansk',
          'seat': '41A',
          'gate': '23B'
        }

        self.assertEqual(expected_value, self.boarding_pass.to_json())
