import unittest
from api import app, db
from api.models import Trip, BoardingPass, MeanOfTransport


class ApiTests(unittest.TestCase):
    def setUp(self):
        # configure app
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../test.sqlite'
        self.app = app.test_client()

        # prepare db
        db.drop_all()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_create_trip(self):
        response = self.app.post(
            path='/api/v1/trips/', json={
                'name': 'poland_trip',
                'boarding_pass':
                [
                    {
                        'origin': 'warszawa',
                        'destination': 'gdansk',
                        'mean_of_transport': 'bus'
                    },
                    {
                        'origin': 'krakow',
                        'destination': 'wroclaw',
                        'mean_of_transport': 'train',
                        'seat': '123'
                    },
                    {
                        'origin': 'gdansk',
                        'destination': 'krakow',
                        'mean_of_transport': 'bus'
                    }
                ]
            }
        )

        expected_value = {
            'name': 'poland_trip',
            'boarding_pass': [
                {
                    'origin': 'warszawa',
                    'destination': 'gdansk',
                    'mean_of_transport': 'bus'
                },
                {
                    'origin': 'gdansk',
                    'destination': 'krakow',
                    'mean_of_transport': 'bus'
                },
                {
                    'origin': 'krakow',
                    'destination': 'wroclaw',
                    'mean_of_transport': 'train',
                    'seat': '123'
                }
            ]
        }
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_value, response.json)

    def test_get_trips(self):
        response = self.app.get(path='/api/v1/trips/')

        self.assertEqual(200, response.status_code)

    def test_get_trip_details(self):
        trip = Trip(name='Poland Trip')
        db.session.add(trip)
        db.session.commit()

        mean_of_transport = MeanOfTransport(id=3,
                                            type_of_transport='airplane')
        db.session.add(mean_of_transport)

        boarding_pass1 = BoardingPass(trip_id=trip.id,
                                      order=1,
                                      mean_of_transport_id=3,
                                      origin='warszawa',
                                      destination='gdansk',
                                      seat='41A',
                                      gate='23B')

        boarding_pass2 = BoardingPass(trip_id=trip.id,
                                      order=2,
                                      mean_of_transport_id=3,
                                      origin='gdansk',
                                      destination='krakow',
                                      seat='22A',
                                      gate='null')

        db.session.add(boarding_pass1)
        db.session.add(boarding_pass2)
        db.session.commit()

        path = '/api/v1/trips/{}'.format(trip.id)
        expected_value = {
            'id': trip.id,
            'name': 'Poland Trip',
            'boarding_passes': [
                {
                    'mean_of_transport': 3,
                    'order': 1,
                    'origin': 'warszawa',
                    'destination': 'gdansk',
                    'seat': '41A',
                    'gate': '23B'
                },
                {
                    'mean_of_transport': 3,
                    'order': 2,
                    'origin': 'gdansk',
                    'destination': 'krakow',
                    'seat': '22A',
                    'gate': 'null'
                }
            ]
        }

        response = self.app.get(path=path)

        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_value, response.json)
