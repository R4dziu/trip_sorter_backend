import unittest
from utils.sorter import Sorter


class SorterTests(unittest.TestCase):

    def setUp(self):
        self.sorter = Sorter()

        self.boarding_passes = [
            {
                'mean_of_transport': 'bus',
                'origin': 'warszawa',
                'destination': 'gdansk',
                'seat': 'null',
                'gate': 'null'
            },
            {
                'mean_of_transport': 'bus',
                'origin': 'krakow',
                'destination': 'wroclaw',
                'seat': 'null',
                'gate': 'null'
            },
            {
                'mean_of_transport': 'train',
                'origin': 'gdansk',
                'destination': 'krakow',
                'seat': '123',
                'gate': 'null'
            }
        ]

    def test_sort(self):
        expected_value = [
            {
                'mean_of_transport': 'bus',
                'origin': 'warszawa',
                'destination': 'gdansk',
                'seat': 'null',
                'gate': 'null'
            },
            {
                'mean_of_transport': 'train',
                'origin': 'gdansk',
                'destination': 'krakow',
                'seat': '123',
                'gate': 'null'
            },
            {
                'mean_of_transport': 'bus',
                'origin': 'krakow',
                'destination': 'wroclaw',
                'seat': 'null',
                'gate': 'null'
            }
        ]

        self.assertEqual(expected_value,
                         self.sorter.sort(self.boarding_passes))

    def test_get_first_and_last_stop(self):
        expected_value = (
            {
                'mean_of_transport': 'bus',
                'origin': 'warszawa',
                'destination': 'gdansk',
                'seat': 'null',
                'gate': 'null'
            },
            {
                'mean_of_transport': 'bus',
                'origin': 'krakow',
                'destination': 'wroclaw',
                'seat': 'null',
                'gate': 'null'
            }
        )

        self.assertEqual(
            expected_value,
            self.sorter.get_first_and_last_stop(self.boarding_passes)
            )

    def test_list_all_arrivals_and_departures(self):
        expected_value = (
            ['gdansk', 'wroclaw', 'krakow'],
            ['warszawa', 'krakow', 'gdansk']
        )

        self.assertEqual(
            expected_value,
            self.sorter.list_all_arrivals_and_departures(self.boarding_passes)
            )
