class Sorter():

    def sort(self, boarding_passes):
        first_stop, last_stop = self.get_first_and_last_stop(boarding_passes)
        results = self.sort_all(boarding_passes, first_stop, last_stop)

        return results

    def get_first_and_last_stop(self, boarding_passes):
        arrival, departure = self.list_all_arrivals_and_departures(
            boarding_passes)

        for boarding_pass in boarding_passes:
            if boarding_pass.get('origin') not in arrival:
                first_stop = boarding_pass
            if boarding_pass.get('destination') not in departure:
                last_stop = boarding_pass

        return first_stop, last_stop

    def list_all_arrivals_and_departures(self, boarding_passes):
        arrival = []
        departure = []

        for boarding_pass in boarding_passes:
            departure.append(boarding_pass.get('origin'))
            arrival.append(boarding_pass.get('destination'))

        return arrival, departure

    def sort_all(self, boarding_passes, first_stop, last_stop):
        sorted_list = []
        sorted_list.append(first_stop)

        while sorted_list[-1] is not last_stop:
            for boarding_pass in boarding_passes:
                last_destination = sorted_list[-1].get('destination')
                current_origin = boarding_pass.get('origin')

                if last_destination == current_origin:
                    sorted_list.append(boarding_pass)
                    boarding_passes.remove(boarding_pass)
                    break

        return sorted_list
